## Kenny Chang
Web Developer

- Email: ananya@gmail.com
- Phone: 0932011007
- GitLab: https://gitlab.com/Kennny

### Summary
Innovative Web Developer with four years of experience building responsive websites, including internal/external system, data visualization and e2e/unit testing. Proficient in HTML, JavaScript, major using React.js & Vue.js as front-end framework, express as back-end framework. Have the ability to communicate and cooperate with others to make the product better. Always strive for efficiency and quality in developing.

### Ability
- Specialize in Vue.js, React.js framework
- Familiar with express
- Familiar with JavaScript(ES6), CSS, STYLUS, HTML (2+ years' experience)
- Experience in E2E testing using puppeteer
- Experience in unit testing using Jest
- Data visualization using highcharts
- Knowledge of git flow

### Experience
**Web developer** @[Innova Solutions](https://www.innovasolutions.com) - Dec. 2019 – Now

- **Responsibilities:**
    - Full stack development and maintain payment management websites.
    - Research and implement with new payment related initiative.
    - Cooperate and code review with TW/US colleagues.
    - API server implementation.
    - Assist in sprint planning.
    - Unit testing.

- **Applied skills**
    - React.js
    - Express
    - Unit test
    - Git, Scrum

**Web developer** @Awesomegaming - Jul. 2019 – Nov. 2019

- **Responsibilities:**
    - Development game platform website run in potato-chat and normal browser.
    - Develop schedule evaluation 

- **Applied skills**
    - Vue.js, React.js
    - Git

**Web developer** @ [Inquartik](https://www.inquartik.com) - Sep. 2017 – Jul.2019

- **Responsibilities:**
    - Develop and maintain a patent analysis website with only one click to show patent value & quality (DD).
    - Develop and maintain a patent transaction website for buyer, seller and broker.
    - Design and implement data visualization charts to show patent information for professional users.
    - Backstage management website implementation.
    - Automatic end to end testing before deployment ( actions from buyer order to deal).
    - Maintain company's official website (Wordpress)

- **Applied skills**
    - HTML, JavaScript, CSS, Stylus, Bootstrap
    - Vue.js, Quasar as framework
    - Highcharts for data visualization
    - Puppeteer E2E testing
    - Git, Scrum

**Software Engineer** @ [Castles Technology](https://www.castlestech.com/zh-hant/), Aug. 2016 – Jun 2017

- **Responsibilities:**
    - Develop and maintain terminal applications.
    - Participate in wolfssl maintenance. (One high level security problem solved)
    - Building TLS server & client. (Server – QT. Client – NetBeans)
    - Testing for key management system from planning to executing.
    - Building website (Terminal Management System).
- **Applied skills**
    - Application : C/C++, Android, QT, TLS, SVN
    - Web : HTML, CSS, JavaScript, jQuery, Bootstrap

**RDAA Intern** @ [Microsoft Taiwan](https://www.microsoft.com/taiwan/campus/), Jul. 2015 – Jun 2016

- **Responsibilities:** 
    - Technical assistant and presales for Office365.
    - Campus lecturer of Office365.
    - Teaching Kodu game lab to kids for basic program logic.
    - Problem solving.
- **Applied skills**
    - Office365, PowerDirector, Kodu

### Education & English

 - **National Central University**, Sep. 2012 - Jun. 2016

    - Bachelor of Computer Science Information Engineering

 - **TOEIC**, Aug. 2016

    - Total: 735 / 990 
